import math

import numpy as np
import sys

sys.path.append(".")

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Dropout
import pylab as pl

np.random.seed(7)

nlayers = 3
nrelus = 12

model = Sequential()
model.add(Dense(nrelus, input_dim=2))
model.add(Activation('relu'))
for layer_n in range(nlayers-1):
    model.add(Dense(nrelus))
    model.add(Activation('relu'))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mse', metrics=['mae'])
print(model.summary())

# sin_a * z

training_count = 500
z_lower_bound = -4.0
z_upper_bound = 4.0
a_lower_bound = -3.0
a_upper_bound = 3.0

multipliers = np.linspace(z_lower_bound, z_upper_bound, training_count).reshape(-1, 1)

angles = np.linspace(a_lower_bound, a_upper_bound, training_count).reshape(-1, 1)
angle_sins = np.sin(angles)
angle_coss = np.cos(angles)

xs, ys = [], []
for z in multipliers:
    for a_i in range(len(angles)):
        xs.append([angles[a_i], z])
        ys.append((angle_sins[a_i] * z))

xs = np.array(xs).reshape(-1, 2)
ys = np.array(ys).reshape(-1, 1)

model.fit(xs, ys, batch_size=32, epochs=100)


validation_count = 5000
xs_valid, ys_valid = [], []
for i in range(validation_count):
    z = np.random.uniform(z_lower_bound, z_upper_bound)
    a = np.random.uniform(a_lower_bound, a_upper_bound)
    xs_valid.append([a, z])
    ys_valid.append((math.sin(a)*z))

xs_valid = np.array(xs_valid)
ys_valid = np.array(ys_valid)

score = model.evaluate(xs_valid, ys_valid, verbose=2)
model.save('x_sin.h5'.format(nlayers, nrelus, score))

print("Test loss:", score[0])
print("Test mae:", score[1])

res = model.predict(xs, batch_size=32)

pl.subplot(211)
pl.plot(res, label='ann')
pl.plot(ys, label='train')
pl.xlabel('#')
pl.ylabel('value [arb.]')
pl.legend()
pl.subplot(212)
pl.plot(ys - res, label='diff')
pl.legend()
pl.savefig("dx_{}_{}_{}.png".format(nlayers, nrelus, score))
#pl.show()
